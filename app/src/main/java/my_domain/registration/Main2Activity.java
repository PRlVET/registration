package my_domain.registration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {




    TextView difficultyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        String strpassword = intent.getStringExtra("name");

        int difficulty = 0;
        int length = 0;

        boolean letter = false;
        boolean diggit = false;
        boolean upperCase = false;
        boolean lowerCase = false;
        boolean symbol = false;


        difficultyView = (TextView) findViewById(R.id.difficulty);

        for (int i = 0; i < strpassword.length(); i++) {
            if (diggit == false) {
                diggit = Character.isDigit(strpassword.charAt(i));
            } else break;
        }
        for (int i = 0; i < strpassword.length(); i++) {
            if (letter == false) {
                letter = Character.isLetter(strpassword.charAt(i));
            } else break;
        }
        for (int i = 0; i < strpassword.length(); i++) {
            if (letter == true) {
                if (upperCase == false)
                    upperCase = Character.isUpperCase(strpassword.charAt(i));
                else break;
            }
        }
        for (int i = 0; i < strpassword.length(); i++) {
            if (letter == true) {
                if (lowerCase == false)
                    lowerCase = Character.isLowerCase(strpassword.charAt(i));
                else break;
            }
        }
        for (int i = 0; i < strpassword.length(); i++) {
            if(!Character.isDigit(strpassword.charAt(i)) && !Character.isLetter(strpassword.charAt(i))) {
                if(symbol == false)
                    symbol = true;
                else break;
            }
        }
            if (strpassword.length() == 6) {
                length = 1;
            }
            if (strpassword.length() > 6 && strpassword.length() < 8) {
                length = 2;
            }
            if (strpassword.length() > 8) {
                length = 3;
            }

        if (symbol == true && diggit == true && upperCase == true && lowerCase == true && length == 1 )
            difficulty = 1;
        if (symbol == true && diggit == true && upperCase == true && lowerCase == true && length == 2)
            difficulty = 2;
        if (symbol == true && diggit == true && upperCase == true && lowerCase == true && length == 3)
            difficulty = 3;

        if (symbol == false && diggit == true && upperCase == true && lowerCase == true && length == 1)
            difficulty = 1;
        if (symbol == false && diggit == true && upperCase == true && lowerCase == true && length == 2)
            difficulty = 2;
        if (symbol == false && diggit == true && upperCase == true && lowerCase == true && length == 3)
            difficulty = 2;

        if (symbol == false && diggit == false && upperCase == true && lowerCase == true && length == 1)
            difficulty = 1;
        if (symbol == false && diggit == false && upperCase == true && lowerCase == true && length == 2)
            difficulty = 1;
        if (symbol == false && diggit == false && upperCase == true && lowerCase == true && length == 3)
            difficulty = 1;

        if (symbol == true && diggit == false && upperCase == false && lowerCase == true && length == 1)
            difficulty = 1;
        if (symbol == true && diggit == false && upperCase == false && lowerCase == true && length == 2)
            difficulty = 1;
        if (symbol == true && diggit == false && upperCase == false && lowerCase == true && length == 3)
            difficulty = 1;

        if (symbol == false && diggit == true && upperCase == false && lowerCase == true && length == 1)
            difficulty = 1;
        if (symbol == false && diggit == true && upperCase == false && lowerCase == true && length == 2)
            difficulty = 1;
        if (symbol == false && diggit == true && upperCase == false && lowerCase == true && length == 3)
            difficulty = 1;

        if (symbol == true && diggit == false && upperCase == true && lowerCase == false && length == 1)
            difficulty = 1;
        if (symbol == true && diggit == false && upperCase == true && lowerCase == false && length == 2)
            difficulty = 1;
        if (symbol == true && diggit == false && upperCase == true && lowerCase == false && length == 3)
            difficulty = 1;

        if (symbol == false && diggit == true && upperCase == true && lowerCase == false && length == 1)
            difficulty = 1;
        if (symbol == false && diggit == true && upperCase == true && lowerCase == false && length == 2)
            difficulty = 1;
        if (symbol == false && diggit == true && upperCase == true && lowerCase == false && length == 3)
            difficulty = 1;

        if (symbol == true && diggit == true && upperCase == false && lowerCase == false && length == 1)
            difficulty = 1;
        if (symbol == true && diggit == true && upperCase == false && lowerCase == false && length == 2)
            difficulty = 1;
        if (symbol == true && diggit == true && upperCase == false && lowerCase == false && length == 3)
            difficulty = 1;

        if (symbol == false && diggit == true && upperCase == true && lowerCase == true && length == 1)
            difficulty = 1;
        if (symbol == false && diggit == true && upperCase == true && lowerCase == true && length == 2)
            difficulty = 2;
        if (symbol == false && diggit == true && upperCase == true && lowerCase == true && length == 3)
            difficulty = 2;

        if (symbol == true && diggit == false && upperCase == true && lowerCase == true && length == 1)
            difficulty = 1;
        if (symbol == true && diggit == false && upperCase == true && lowerCase == true && length == 2)
            difficulty = 2;
        if (symbol == true && diggit == false && upperCase == true && lowerCase == true && length == 3)
            difficulty = 2;

        if (symbol == true && diggit == true && upperCase == false && lowerCase == true && length == 1)
            difficulty = 1;
        if (symbol == true && diggit == true && upperCase == false && lowerCase == true && length == 2)
            difficulty = 2;
        if (symbol == true && diggit == true && upperCase == false && lowerCase == true && length == 3)
            difficulty = 2;

        if (symbol == true && diggit == true && upperCase == true && lowerCase == false && length == 1)
            difficulty = 1;
        if (symbol == true && diggit == true && upperCase == true && lowerCase == false && length == 2)
            difficulty = 2;
        if (symbol == true && diggit == true && upperCase == true && lowerCase == false && length == 3)
            difficulty = 2;


            switch (difficulty) {
                case 1:
                    difficultyView.setText("Легко");
                    break;
                case 2:
                    difficultyView.setText("Средне");
                    break;
                case 3:
                    difficultyView.setText("Сложно");
                    break;
            }
    }
}


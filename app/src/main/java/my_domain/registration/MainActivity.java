package my_domain.registration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button RegButton;
    EditText login;
    EditText password;
    EditText rpassword;
    TextView elogin;
    TextView epassword;
    TextView erpassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RegButton = (Button) findViewById(R.id.RegButton);
        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);
        rpassword = (EditText) findViewById(R.id.rpassword);
        elogin = (TextView) findViewById(R.id.elogin);
        epassword = (TextView) findViewById(R.id.epassword);
        erpassword = (TextView) findViewById(R.id.erpassword);
        RegButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        String strlogin = login.getText().toString();
        String strpassword = password.getText().toString();
        String strrpassword = rpassword.getText().toString();

        boolean blogin;
        boolean bpassword;
        boolean brpassword = false;
        if (strlogin.length() == 0){
            elogin.setText("Введите логин");
            blogin = false;
        }else{
            elogin.setText("");
            blogin = true;}

        if (strpassword.length() == 0)
        {   epassword.setText("Введите пароль");}

        if (strpassword.matches("^[а-я]+") || strpassword.matches("^[А-Я]+") || strpassword.matches("^[0-9]+") || strpassword.matches("^[a-z]+") || strpassword.matches("^[A-Z]+") || strpassword.matches("^[\\s]+") || strpassword.matches("^[\\W]+"))
        { epassword.setText("Пароль должен состоять из 2-х или более групп символов");
        bpassword = false;}
        else {
            epassword.setText("");
            bpassword = true; }

        if (strpassword.length() < 6)
        {   epassword.setText("Пароль должен быть больше 6-ти символов");
        bpassword = false;}


        if (strpassword.length() != 0) {
            if (strpassword.length() == strrpassword.length()) {
                for (int count = 0; count < strpassword.length(); count++) {
                    if (strpassword.charAt(count) == strrpassword.charAt(count)) {
                        erpassword.setText("");
                        brpassword = true;
                    } else {
                        erpassword.setText("пароли должны совпадать");
                        brpassword = false;
                    }
                }
            } else { erpassword.setText("Пароли должны совпадать");
            brpassword = false;}
        } else erpassword.setText("");

        Intent intent = new Intent(this,Main2Activity.class);

       if (blogin == true && bpassword == true && brpassword == true) {
           intent.putExtra ("name",password.getText().toString());
           startActivity(intent); }




    }
}
